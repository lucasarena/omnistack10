import React, { useEffect, useState } from 'react';

import api from '../../../services/api';
import DevForm from '../../../components/DevForm';
import './styles.css';

function Edit(props) {
    const [id, setId] = useState('');
    const [github_username, setGithubUsername] = useState('');
    const [techs, setTechs] = useState('');
    const [latitude, setLatitude] = useState('');
    const [longitude, setLongitude] = useState('');

    async function getDev() {
        const id = props.match.params.id;    
        const { data: { github_username: name, techs, location: { coordinates } } } = await api.get(`/devs/${id}`);           
        setId(id);
        setGithubUsername(name);
        setTechs(techs.join(', '));
        setLongitude(coordinates[0]);
        setLatitude(coordinates[1]);
    }

    useEffect(() => {
        getDev();
    }, []);

    async function updateDev({github_username, techs, latitude, longitude}) {
        await api.put(`/devs/${id}`, {
            github_username,
            techs,
            latitude,
            longitude
        });

        props.history.push('/');
    }

    return (
        <div id="container">
            <main id="content">
                <strong>Editar</strong>
                <DevForm                    
                    github_username={github_username}
                    techs={techs}
                    latitude={latitude}
                    longitude={longitude}
                    button="Editar"
                    onSubmit={updateDev}
                />
            </main>
        </div>
    );
}

export default Edit;