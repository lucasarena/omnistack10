import React, {useState, useEffect } from 'react';

import api from '../../services/api';
import DevItem from '../../components/DevItem';
import DevForm from '../../components/DevForm';

import './Main.css';
import './SideBar.css';

function Main() {
    const [devs, setDevs] = useState([]);
    
    useEffect(() => {
    async function loadDevs() {
        const response = await api.get('/devs');

        setDevs(response.data.devs); 
    }
    loadDevs();
    }, []);

    async function handleAddDev(data) {
        try {  
            const response = await api.post('/devs', data);
            setDevs([...devs, response.data.dev]);
        } catch (error) {
            console.log(error);
        }
    }

    return (
        <div id="app">    
            <aside>
            <strong>Cadastrar</strong>
                <DevForm 
                    onSubmit={handleAddDev}
                    button="Salvar"
                />
            </aside>
            <main>
            <ul>
                {devs.map(dev => (
                <DevItem key={dev._id} dev={dev} />
                ))} 
            </ul>
            </main>
        </div>
    );
}

export default Main;