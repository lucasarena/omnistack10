import React from 'react';

import './styles.css';

function DevItem({ dev }) {
    return (
        <li className="dev-item" key={dev._id}>
            <header>
                <div className="info">
                    <img src={dev.avatar_url} alt={dev.name}/>
                    <div className="user-info">
                        <strong>{dev.name}</strong>
                        <span>{dev.techs.join(', ')}</span>
                    </div>
                </div>
                <div><a href={`/devs/edit/${dev._id}`} >Editar</a></div>
            </header>
            <p>{dev.bio}</p>
            <a href={`https://github.com/${dev.github_username}`} target="black">Acessar perfil no github</a>
        </li>
    );
}

export default DevItem;