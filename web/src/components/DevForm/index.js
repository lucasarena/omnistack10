import React, { useState, useEffect } from 'react';
import './styles.css';

function DevForm({ onSubmit, github_username: username, techs: devTech, devTechs, button }) {

    const [github_username, setGitbubUsername] = useState('');
    const [techs, setTechs] = useState(devTechs || '');
    const [latitude, setLatitude] = useState('');
    const [longitude, setLongitude] = useState('');
    
    useEffect(() => {
        navigator.geolocation.getCurrentPosition(
          (position) => {
            const { longitude, latitude } = position.coords;
    
            setLatitude(latitude);
            setLongitude(longitude);
          },
          (error) => {
            console.log(error);
          },
          {
            timeout: 30000
          }
        )
      }, []);

    useEffect(() => {
      setGitbubUsername(username);
      setTechs(devTech);          
    },[username,devTech])

    async function handleSubmit(event) {
        event.preventDefault();        
        await onSubmit({
            github_username,
            techs,
            latitude,
            longitude
          });

          setGitbubUsername('');
          setTechs('');    
    }

    return (
        <form onSubmit={handleSubmit}>
          <div className="input-block">
            <label htmlFor="github_username">Usuário do Github</label>
            <input 
              name="github_username" 
              className="input-full" 
              id="github_username"  
              required
              onChange={event => setGitbubUsername(event.target.value)}
              value={github_username}
            />
          </div>

          <div className="input-block">
            <label htmlFor="techs">Tecnológias</label>
            <input 
              name="techs" 
              id="techs" 
              className="input-full" 
              required
              onChange={event => setTechs(event.target.value)}
              value={techs}
            />
          </div>

          <div className="input-group">
            <div className="input-block">
              <label htmlFor="latitude">Latitude</label>
              <input 
                type="number" 
                name="latitude" 
                id="latitude" 
                onChange={event => setLatitude(event.target.value)} 
                value={latitude} 
                required 
              />
            </div>
            <div className="input-block">
              <label htmlFor="logitude">Logitude</label>
              <input 
                type="number" 
                name="logitude" 
                id="logitude" 
                onChange={event => setLongitude(event.target.value)} 
                value={longitude}  
                required 
              />
            </div>
          </div>
          <button 
            type="submit"
          >{button}</button>
        </form>
    );
}

export default DevForm;