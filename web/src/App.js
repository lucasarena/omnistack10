import React from 'react';
import { Router, Switch, Route } from 'react-router-dom'
import { createBrowserHistory } from 'history'

import Main from './views/Main/index';
import Edit from './views/Main/Edit';
import './global.css';
import './App.css';

function App() {
  const browserHistory = createBrowserHistory();
  return (
    <Router history={browserHistory}>
      <Switch>
        <Route path="/devs/edit/:id" component={Edit} />
        <Route path="/" component={Main} />
      </Switch>
    </Router>
  )
}

export default App;
