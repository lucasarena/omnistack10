const axios = require('axios');
const Dev = require('../models/dev');

const parseStringAsArray = require('../utils/parseStringAsArray');
const { findConnectios, sendMessage } = require('../websocket');

module.exports = {
    async index(req, res) {
        const devs = await Dev.find();
        res.json({ devs });
    },

    async find(req, res) {
        const { id } = req.params;
        const dev = await Dev.findById(id);

        res.json(dev);
    },

    async store(req, res) {
        const { github_username, techs, latitude, longitude } = req.body;

        let dev = await Dev.findOne({ github_username });
    
        if (!dev) {
            const response = await axios.get(`https://api.github.com/users/${github_username}`);
        
            const { login, avatar_url, bio } = response.data;
        
            const location = {
                type: 'Point',
                coordinates: [longitude, latitude]
            }

            const techsArray = parseStringAsArray(techs);
                
            dev = await Dev.create({
                name: login, 
                avatar_url,
                bio, 
                github_username,
                techs: techsArray,
                location
            });

            const sendMessageTo = findConnectios({ latitude, longitude }, techsArray);
            sendMessage(sendMessageTo, 'new-dev', dev);
        }
    
        res.json({ dev })
    },

    async update(req, res) {
        const { techs, latitude, longitude } = req.body;
        const { id } = req.params;

        const arrayTechs = parseStringAsArray(techs);

        const dev = await Dev.findByIdAndUpdate(id, {
            techs: arrayTechs,
            latitude, 
            longitude
        }, { new: true });

        res.json({ dev });
    },
    
    async delete(req, res) {
        const { id } = req.params;
        await Dev.findByIdAndDelete(id);

        res.json({
            'message' : 'Usuário deletado'
        })
    }
}