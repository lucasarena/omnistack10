const mongoose = require('mongoose');

const PointSchema = new mongoose.Schema({
    type: {
        type: String,
        required: true,
        enum: ['Point']
    },
    coordinates: {
        type: [Number],
        required: true
    }
});

module.exports = PointSchema;